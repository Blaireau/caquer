import re
import os
from pathlib import Path

# automate de lecture
ETATS_POSSIBLES = ["filtre", "nombre", "filtre", "lettre"]
NEWLINES = ["\n", "\r"]
SEPARATION_FICHIER = "# SEPARATION"
BALISE_INSERTION_LATEX = "% INSERTION DES REMARQUES"
BALISE_ATTENDRE_RENDU = "[DEVOIR À RENDRE]"
REGEX_FORMAT_FICHIER = re.compile(r"([\s\W]*(\d+[\s\W]*([\s\W][a-zA-Z][\s\W]*)?)?)?\n")
REGEX_DOSSIER_ELEVE = re.compile(r"([\w-]+) ([A-Z -]+)_\d+_assignsubmission_file.*")

# dossier / système de fichiers
USERHOME = Path.home().as_posix()
#
ROOT = Path(__file__).parent
DOSSIER_QCM = ROOT / "qcm/"
DOSSIER_COLLECTE = ROOT / "collecte/"
DOSSIER_EXPORT_PDF = ROOT / "pdf_export/"
ARCHIVE_CORRECTIONS = "devoirs-corriges.zip"
BASE_LATEX = ROOT / "base.tex"
LATEX_LOG = ROOT / "latexOutput"
NOM_SYNTHESE = "[synthèse]"
DUMMY_STUDENT = 'Jean CERRIEN_0000_assignsubmission_file_'

# affichage
TAILLE_LIGNE_TERMINAL = 60

# pdf résultats
TAILLE_MAX_TABLEAU = 15
LATEX_TIMEOUT = 5 # 5 sec

