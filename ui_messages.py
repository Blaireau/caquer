from constantes import TAILLE_LIGNE_TERMINAL

MESSAGES = {
'welcome': "CAQUER :\nCorrecteur Automatiqe de QCM, Universel Épatant et Révolutionnaire",
'no-qcm': 'Veuillez choisir un fichier de QCM (.qcm)',
'not-a-qcm': 'Veuillez choisir un fichier ".qcm" pour le QCM',
'no-archive': 'Veuillez choisir un fichier .zip contenant les devoirs à corriger',

'latex-error': "Latex ne répond pas. Le code Latex généré est probablement invalide.\nRegardez le fichier de log et le code latex généré ('__.tex') pour plus de détails.",



# older messages

'cmd-not-found': "La commande {command} n'existe pas.",

'qcm-file-found': "Fichier qcm trouvé : '{path}'",

'invalid-arg-count': "\n".join([

        "Nombre d'arguments invalides pour la commande {command}.",
        "La commande attendait {expectedArgs} arguments, seulement {foundArgs} fournis."
]),

'status': "QCM : {qcmInfo} ",

'file-not-found': "Le fichier '{file}' n’existe pas.",

'homework-archive-not-found': "Aucune archive zip contenant des devoirs dans le dossier '{folder}'",

'missing-qcm': "Sélectionnez un qcm.",

'test-solution-generated': "Correction avec toutes les indications possibles générée",

'extract-answer': "Extraction des réponses...",

'grade-answer': "Évaluation des devoirs...",

'student-entry': "Élève {student} : ",

'student-ignore': "Ignorer {student} : ",

'help-bloc': "\n".join([

        "-"*TAILLE_LIGNE_TERMINAL,
        "Aide de la commande '{command}' :",
        "{text}",
        "-"*TAILLE_LIGNE_TERMINAL
]),

'list-cmd': "Liste des commandes disponibles : \n{text}",

'nb-questions': "Nombre de questions pour le qcm : ",

'grading-student': "Notation du devoir de {student}",

'file-to-process': "{total} fichiers à analyser",

'invalid-syntax': "\n".join([
        "La syntaxe de ce fichier est incorrect ligne {line}.",
        "Veuillez vérifier ce fichier puis continuer.",
        "(recommencer/ignorer)"
]),

'invalid-format': "\n".join([
        "Erreur lors de la lecture du fichier.",
        "Le format de ce fichier est invalide.",
        "L'élève a probablement rendu un fichier sous un autre format que .txt",
        "(.pdf, .odt, .docx)\n(recommencer/ignorer)"
]),

'ask-continue-grading': "Voulez-vous continuer la correction pour les autres élèves ? (oui/non)",

'grading-error': "Une erreur s'est produite. La correction doit être stoppée.",

'generating-pdf': "Générer pdf '{name}'…",


'analysing-file': "Analyse de {file}",

}
