import json
import statistics
import subprocess
import os
from pathlib import Path

from constantes import *
from reponses import ReponsesStatus
from outils import *
from errors import UserError

def calculer_note(juste, faux, blanc, nbQuestions):
    """ Calcule la note au format (nbPointsObtenus, nbPointsTotal) """
    score = max(4 * juste - faux, 0)
    return score, 4 * nbQuestions

def afficher_points(resultat):
    """ Format d'affichage des points """
    if resultat == "juste":
        return "+4"
    elif resultat == "faux":
        return "-1"
    else:
        return "0"


class QCM:
    """ Permet de modéliser un test QCM """

    def __init__(self, fichierQCM, nouveau=False, **kwargs):
        """ Crée un QCM à partir d'un fichier 'fichierQCM' """
        self.fichier = fichierQCM
        if nouveau:
            self.nbQuestions = kwargs["nbQuestions"]
            self.titre = extraire_nom(fichierQCM)
            self.solutions = ["" for _ in range(self.nbQuestions)]
            self.indications = [f"Indication {i+1}" for i in range(self.nbQuestions)]
            self.remarques = {
                "40": "Insuffisant !",
                "50": "À consolider.",
                "60": "Satisfaisant.",
                "80": "Bien !",
                "100": "Très bien !"
            }
            self.sauver()
        else:
            self.ouvrir()

    def ouvrir(self):
        with open(self.fichier, "r") as fQCM:
            data = fQCM.read()
        titre, dataQCM, dataRemarques = data.split(SEPARATION_FICHIER)
        self.titre = eval(titre)
        self.solutions = []
        self.indications = []
        for elem in eval(dataQCM):
            solution, indication = elem
            self.solutions.append(solution)
            self.indications.append(indication)
        self.remarques = eval(dataRemarques)
        self.nbQuestions = len(self.solutions)

    def sauver(self):
        """ Enregistre le QCM dans son fichier d'origine """
        with open(self.fichier, "w") as fQCM:
            fQCM.write(str(self))

    def __repr__(self):
        """ Permet de représenter le QCM pour mprint et enregistrement """
        data = []
        for sol, ind in zip(self.solutions, self.indications):
            data.append(f"""
\t[
\t\t\"{sol}\",
\t\tr\"\"\"{ind}\"\"\"
\t],""")
        s0 = json.dumps(self.titre, indent=4, ensure_ascii=False)
        s1 = "[\n"+"".join(data)[:-1]+"\n]"
        s2 = json.dumps(self.remarques, indent=4, ensure_ascii=False)
        sep = SEPARATION_FICHIER
        return "\n".join([s0, sep, s1, sep, s2])

    def générer_titre_latex(self):
        """ Renvoie le code latex du titre du QCM """
        t = [
        r"\huge",
        f"{self.titre}"+r"\\",
        r"\vspace{1cm}",
        r"\normalsize",
        "\n"
        ]
        return "\n".join(t)

    def générer_synthèse_latex(self, note, résultats):
        """ Renvoie le code latex du total des questions bien/mal répondues """
        j = résultats.count("juste")
        f = résultats.count("faux")
        b = résultats.count("")
        t = [
        f"Bonnes réponses : {j}"+r"\\",
        f"Réponses incorrectes : {f}"+r"\\",
        f"Questions non traitées : {b}"+r"\\",
        "Votre note est {} / {}".format(*note)+r"\\"
        ]
        pourcentage = 100 * note[0] / note[1]
        remarque = ""
        dernierSup = 101
        for borneSupStr, rem in self.remarques.items():
            borneSup = int(borneSupStr)
            if pourcentage <= borneSup and borneSup < dernierSup:
                remarque = rem
                dernierSup = borneSup
        t.append(remarque)
        return "\n".join(t)

    def générer_tableau_résultats_latex(self, réponses, résultats):
        """ Renvoie le code latex du tableau récapitulatif des réponses """
        t = []
        for débutLigneQu in range(0, self.nbQuestions, TAILLE_MAX_TABLEAU):
            maxiLigne = débutLigneQu + TAILLE_MAX_TABLEAU
            maxiLigne = maxiLigne if maxiLigne <= self.nbQuestions else self.nbQuestions
            taille = maxiLigne - débutLigneQu
            t.extend([
        r"\begin{table}[H]",
        r"\begin{tabular}{|c|"+"c|"*taille+"}",
        r"\hline",
        r"Question "+"".join([f"& {i+1} "
                              for i in range(débutLigneQu, maxiLigne)])+r"\\",
        r"\hline",
        r"Réponse donnée "+"".join([f"& {rep} "
                                    for rep in réponses[débutLigneQu:maxiLigne]])+r"\\",
        r"\hline",
        r"Points "+"".join([f"& {afficher_points(res)} "
                            for res in résultats[débutLigneQu:maxiLigne]])+r"\\",
        r"\hline",
        r"\end{tabular}",
        r"\end{table}"
            ])
        return "\n".join(t)

    def générer_indications_latex(self, résultats):
        """ Renvoie le code latex des conseils pour les questions pas réuissies """
        t = []
        for numQu, résultat in enumerate(résultats):
            if résultat != "juste":
                avant = r"\begin{minipage}{\linewidth}"
                après = r"\end{minipage}"
                t.append(f"{avant}\n{self.indications[numQu]}\n{après}")
        if len(t) == 0:
            return ""
        sep = "\n"*2
        return sep.join(t) + sep

    def évaluer_devoir_élève(self, élève, baseLatex):
        if élève.réponses.status != ReponsesStatus.VALID:
            return
        réponses = élève.réponses.get_réponses_list(self.nbQuestions)
        info('grading-student', student=élève.présenter())
        juste = 0
        faux = 0
        blanc = 0
        résultat = ["" for _ in range(self.nbQuestions)]
        for numQuestion in range(self.nbQuestions):
            réponse = réponses[numQuestion]
            solution = self.solutions[numQuestion]
            if réponse.upper() == solution.upper():
                juste += 1
                résultat[numQuestion] = "juste"
                self.réponses[numQuestion]["juste"] += 1
            elif réponse == "":
                blanc += 1
                self.réponses[numQuestion]["blanc"] += 1
            else:
                faux += 1
                résultat[numQuestion] = "faux"
                self.réponses[numQuestion]["faux"] += 1
        note = calculer_note(juste, faux, blanc, self.nbQuestions)
        élève.note = note
        titre = self.générer_titre_latex()
        entete = élève.générer_entete_latex()
        synthèse = self.générer_synthèse_latex(note, résultat)
        tableau = self.générer_tableau_résultats_latex(réponses, résultat)
        indications = self.générer_indications_latex(résultat)
        codeInséré = "\n".join([titre, entete, synthèse, tableau, indications])
        élève.fichierRemarques = self.générer_pdf_remarques(codeInséré, élève.dossierDevoir.name,
                                                            élève.nomFichier, baseLatex)

    def évaluer_devoir(self, classe, baseLatex, compute_all=True):
        """ Évalue le devoir d'une classe et crée les fichiers de résultats
            à partir d'une base en Latex 'baseLatex' """
        TYPES_REPONSES = "juste", "faux", "blanc"
        if compute_all:
            nettoyer_dossier(DOSSIER_EXPORT_PDF, ["*"])
            self.réponses = [{key: 0 for key in TYPES_REPONSES} for _ in range(self.nbQuestions)]
            for élève in classe:
                self.évaluer_devoir_élève(élève, baseLatex)
        # pdf récapitulatif général
        notesBrutes = [e.note for e in classe]
        contenu = self.générer_latex_synthèse_professeur(notesBrutes, classe)
        nom = f"{NOM_SYNTHESE}"
        return self.générer_pdf_remarques(contenu, '', nom, baseLatex)

    def générer_latex_synthèse_professeur(self, notesBrutes, classe):
        stats = {}
        tuplesFiltered = [note for note in notesBrutes if note is not None]
        notesFiltered = [note[0] for note in tuplesFiltered]
        def normalise_note(note):
            return round(note[0] * 20 / note[1], 2)
        stats["Minimum"] = min(notesFiltered)
        stats["Moyenne"] = round(statistics.mean(notesFiltered), 2)
        stats["Médiane"] = round(statistics.median(notesFiltered), 2)
        stats["Maximum"] = max(notesFiltered)
        nonRendus = notesBrutes.count(None)

        contenu = []
        titre = self.générer_titre_latex()
        contenu.append(titre)

        contenu.append(r"\subsection*{Statistiques globales}")
        contenu.append(r"""
\begin{table}[H]
\begin{tabular}{|l|c|c|}
\hline
 & Note & Note normalisée \\
\hline
""")
        fac = tuplesFiltered[0][1]
        contenu.extend(["\n".join([rf"{key} & {val} / {fac} & {normalise_note((val, fac))} / 20 \\",
                                   r"\hline"])
                        for key, val in stats.items()])
        contenu.append(r"""
\end{tabular}
\end{table}
""")
        contenu.append(f"Non rendus: {nonRendus} / {len(notesBrutes)}")
        contenu.append(r"\subsection*{Notes des élèves}")
        contenu.append(r"""
\begin{table}[H]
\begin{tabular}{|l|c|c|}
\hline
Élève & Note & Note normalisée \\
\hline
""")
        for élève in sorted(classe, key=lambda e: e.nom):
            if élève.note is None:
                note, noteNormalise = ["non rendu"] * 2
            else:
                note = f"{élève.note[0]:2} / {élève.note[1]}"
                noteNormalise = f"{normalise_note(élève.note):2} / 20"
            contenu.extend([
                rf"{élève.présenter()} & {note} & {noteNormalise} \\",
                r"\hline"
                ])
        contenu.append(r"""
\end{tabular}
\end{table}
""")
        contenu.append(r"\subsection*{Statistiques par question}")
        contenu.append(r"""
\begin{table}[H]
\begin{tabular}{|r|c|c|c|}
\hline
Question & Juste & Faux & Blanc \\
\hline
""")
        for qu_n, réponses in enumerate(self.réponses):
            contenu.extend([
                r"{qu_n} & {juste} & {faux} & {blanc} \\".format(qu_n=qu_n + 1, **réponses),
                r"\hline"
                ])
        contenu.append(r"""
\end{tabular}
\end{table}

Note : les statistiques affichées ne concernent que les élèves qui ont rendu le devoir.
""")
        return "\n".join(contenu)

    def générer_pdf_remarques(self, codeInséré, nomDossier, nomEleve, baseLatex):
        """ Génère un document pdf avec le contenu 'codeInséré' et le nom 'nomEleve' """
        # générer le code complet avec la base
        try:
            with open(baseLatex, "r") as fLatex:
                contenuBase = fLatex.read()
        except FileNotFoundError:
            error('file-not-found', file=baseLatex)
            return False
        contenuListe = contenuBase.split(BALISE_INSERTION_LATEX)
        contenuListe.insert(1, codeInséré)
        contenu = "".join(contenuListe)
        # écrire le code dans un fichier temporaire
        nomSourceTemporaire = ROOT/"__.tex"
        with open(nomSourceTemporaire, "w") as fSource:
            fSource.write(contenu)
        nomPdf = f"{nomEleve}_{format_nom_fichier(self.titre)}.pdf"
        dossier = DOSSIER_EXPORT_PDF / Path(nomDossier)
        if not dossier.exists():
            os.mkdir(dossier)
        fullPdfPath = dossier / nomPdf
        # compiler le code pour créer le pdf
        info('generating-pdf', name=nomPdf)
        try:
            with open(LATEX_LOG, "w") as outputFile:
                subprocess.run(["pdflatex",
                                '-output-directory', ROOT,
                                '-interaction=nonstopmode',
                                 nomSourceTemporaire], stdout=outputFile,
                               timeout=LATEX_TIMEOUT)
        except subprocess.TimeoutExpired:
            raise UserError('latex-error')
        subprocess.run(["cp", ROOT/"__.pdf", fullPdfPath.as_posix()])
        nettoyer_dossier(ROOT, ["__.aux", "__.log", "__.out"])
        return fullPdfPath.as_posix()
