import glob
import re
import unidecode
import zipfile
from pathlib import Path

from classes import Eleve
from constantes import *
from outils import *
from qcm import QCM
from reponses import Reponses, ReponsesStatus
from errors import UserError


class Automate:
    """ Gère le traitement des commandes et les liens entre Classe, Eleve et QCM """

    def __init__(self):
        """Création d'un automate de traitement"""
        self.qcmCourant = None
        self.archivefile = None
        self.classeCourrante = None
        self.fichierSynthese = ''

    def extraire_réponses_classe(self, dossierRéponses, archivefile):
        """ Extrait tous les devoirs de la classe courante contenus dans 'dossierRéponses' """
        nettoyer_dossier(DOSSIER_COLLECTE, ["*"])
        self.archivefile = None
        try:
            with zipfile.ZipFile(archivefile, 'r') as file:
                file.extractall(DOSSIER_COLLECTE)
        except FileNotFoundError:
            raise UserError('no-archive')
        self.archivefile = archivefile
        dossierDevoir = [dossier for dossier in Path(dossierRéponses).iterdir()
                                 if Path(dossier).is_dir()]
        info('file-to-process', total=len(dossierDevoir))
        classe = []
        for dossier in dossierDevoir:
            élève = Eleve(dossier)
            élève.extraire_réponses()
            classe.append(élève)
        return classe

    def recorriger_élève(self, élève, classe):
        if élève.réponses.status == ReponsesStatus.VALID:
            return
        élève.extraire_réponses()
        self.qcmCourant.évaluer_devoir_élève(élève, BASE_LATEX)
        self.fichierSynthese = self.qcmCourant.évaluer_devoir(classe, BASE_LATEX, compute_all=False)
        self.archiver_corrections(self.archivefile)

    def vérifier_qcm(self, qcmfile):
        self.ouvrir(qcmfile)
        if self.qcmCourant is None:
            raise UserError('no-qcm')
        eleve = Eleve(Path(DUMMY_STUDENT))
        eleve.réponses = Reponses(fake=True)
        eleve.réponses.data = {n:"" for n in range(self.qcmCourant.nbQuestions)}
        classe = [eleve]
        self.qcmCourant.évaluer_devoir(classe, BASE_LATEX)
        info('test-solution-generated')
        return eleve.fichierRemarques

    def ouvrir(self, nomFichier):
        self.qcmCourant = None
        if nomFichier is None or nomFichier == '':
            return False
        if not nomFichier.endswith('.qcm'):
            raise UserError('not-a-qcm')
        try:
            self.qcmCourant = QCM(nomFichier)
        except FileNotFoundError:
            error('file-not-found', file=nomFichier)
            return False
        return True

    def corriger(self, qcmfile, archivefile):
        self.classeCourante = None
        self.ouvrir(qcmfile)
        if self.qcmCourant is None:
            raise UserError('no-qcm')
        info('extract-answer')
        self.classeCourante = self.extraire_réponses_classe(DOSSIER_COLLECTE, archivefile)
        info('grade-answer')
        self.fichierSynthese = self.qcmCourant.évaluer_devoir(self.classeCourante, BASE_LATEX)
        self.archiver_corrections(archivefile)

    def archiver_corrections(self, archivefile):
        directory = Path(DOSSIER_EXPORT_PDF)
        archivePath = Path(archivefile).parent / ARCHIVE_CORRECTIONS
        with zipfile.ZipFile(archivePath.as_posix(), "w") as zip_file:
            for studentFolder in directory.glob("*"):
                if studentFolder.is_dir():
                    zip_file.write(studentFolder, studentFolder.relative_to(directory))
                    for entry in studentFolder.rglob("*"):
                        zip_file.write(entry, entry.relative_to(directory))
