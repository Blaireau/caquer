import glob
import os
import pathlib
import subprocess
from termcolor import cprint

from ui_messages import MESSAGES

def mprint(*args, **kwargs):
    color = args[-1]
    if color == "":
        print(*args[:-1], **kwargs)
    else:
        cprint(*args[:-1], color, **kwargs)

def ui_message(messageKey, **kwargs):
    return MESSAGES[messageKey].format(**kwargs)

def _user_message(messageKey, color='', **substitutes):
    mprint(MESSAGES[messageKey].format(**substitutes), color)

def info(messageKey, **substitutes):
    _user_message(messageKey, **substitutes)

def warning(messageKey, **substitutes):
    _user_message(messageKey, **substitutes, color='yellow')

def question(messageKey, **substitutes):
    _user_message(messageKey, **substitutes)

def error(messageKey, **substitutes):
    _user_message(messageKey, **substitutes, color='red')

def status(**substitutes):
    _user_message('status', **substitutes, color='green')

def extraire_nom(chemin):
    """Extrait le nom d'un fichier en supprimant le chmin d'accès et l'extension"""
    return pathlib.Path(chemin).stem

def format_nom_fichier(nom):
    resultat = nom.replace(" ", "_")
    return resultat

def chemin_fichier(nomFichier, extension, dossierDefaut, nouveau=False):
    dossierDefaut = pathlib.Path(dossierDefaut)
    if not nomFichier.endswith(extension):
        nomFichier += extension
    if "/" in nomFichier and (os.path.exists(nomFichier) or nouveau):
        return nomFichier
    elif nouveau:
        return dossierDefaut.joinpath(nomFichier)
    else:
        files = glob.glob(str(dossierDefaut.joinpath("**/", nomFichier)), recursive=True)
        if len(files) == 0:
            raise FileNotFoundError(f"Le fichier {nomFichier} n’existe pas")
        return files[0]

def nettoyer_dossier(dossier, motifs):
    for m in motifs:
        for fichier in pathlib.Path(dossier).glob(m):
            if fichier.is_dir():
                for f in fichier.rglob("*"):
                    os.remove(f)
                os.rmdir(fichier)
            else:
                try:
                    os.remove(fichier)
                except OSError:
                    pass

def xdg_open(fichier):
    if fichier:
        subprocess.Popen(['xdg-open', fichier])
