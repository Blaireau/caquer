from constantes import *
from outils import *

from reponses import Reponses

class Eleve:
    """ Simule un élève """

    def __init__(self, dossier):
        """ Crée un élève depuis un dictionnaire de données """
        self.dossierDevoir = dossier
        match = re.match(REGEX_DOSSIER_ELEVE, dossier.name)
        if match:
            self.prénom = match.group(1)
            self.nom = match.group(2)
        else:
            self.nom = '<not found>'
            self.prénom = '<not found>'
        self.réponses = None
        self.__note = None
        self.fichierRemarques = None

    @property
    def note(self):
        return self.__note

    @note.setter
    def note(self, value):
        self.__note = value

    def présenter(self):
        return f"{self.nom} {self.prénom}"

    def extraire_réponses(self):
        try:
            cheminFichier = next(self.dossierDevoir.iterdir())
        except StopIteration:
            self.réponses = Reponses()
            return
        self.réponses = Reponses(cheminFichier)


    @property
    def nomFichier(self):
        """ Renvoie le nom de fichier attendu pour cet élève, sans extension """
        return f"{self.prénom} {self.nom}"

    def générer_entete_latex(self):
        """ Crée un en-tête de présentation de l'élève """
        présentation = f"{self.prénom} {self.nom}"
        return r"\textbf{"+présentation+r"} \\"+"\n"
