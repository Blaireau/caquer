from enum import Enum

from outils import *
from constantes import *

class ReponsesStatus(Enum):
    VALID = 0
    INVALID_FILE_FORMAT = 1
    INVALID_SYNTAX = 2
    NO_FILE = 3



class Reponses:

    def __init__(self, fichierReponses=None, fake=False):
        if fake:
            self.fichier = None
            self.status = ReponsesStatus.VALID
        else:
            if fichierReponses:
                self.fichier = fichierReponses
                self.status = ReponsesStatus.VALID
                self.extraire_réponses()
            else:
                self.fichier = None
                self.status = ReponsesStatus.NO_FILE

    @property
    def cheminFichier(self):
        if self.fichier is None:
            return ''
        return self.fichier.as_posix()

    def get_réponses_list(self, nbQuestions):
        output = []
        for qu in range(nbQuestions):
            try:
                val = self.data[qu]
            except KeyError:
                val = ''
            output.append(val)
        return output

    def vérifier_format(self, texte):
        lignes = texte.split("\n")
        for lNum, ligne in enumerate(lignes):
            if re.match(REGEX_FORMAT_FICHIER, ligne+"\n") is None:
                return False, lNum+1 #car numérotation des lignes à partir de 1
        return True, 0

    def état_suivant(self):
        """ Incrémente l'état de l'automate et effectue les actions
            de changement d'état """
        if self.état == "nombre":
            self.indexRéponse = int("".join(self.buffer)) - 1
            self.buffer = []
        elif self.état == "lettre":
            self.lettre = "".join(self.buffer).upper()
            self.buffer = []
            try:
                self.data[self.indexRéponse] = self.lettre
            except IndexError:
                pass
        self.indexEtat = (self.indexEtat + 1) % len(ETATS_POSSIBLES)
        self.état = ETATS_POSSIBLES[self.indexEtat]

    def état_initial(self):
        """ Remet l'automate à l'état initial """
        self.indexEtat = 0
        self.état = ETATS_POSSIBLES[0]
        self.buffer = []

    def extraire_réponses(self):
        """Extrait la liste de réponses du devoir"""
        info('analysing-file', file=self.fichier.as_posix())
        try:
            with open(self.fichier, "r") as f:
                contenu = f.read()
        except FileNotFoundError:
            error('file-not-found', file=self.fichier.as_posix())
            return
        except UnicodeDecodeError:
            warning('invalid-format')
            self.status = ReponsesStatus.INVALID_FILE_FORMAT
            return
        formatOk, ligne = self.vérifier_format(contenu)
        if not formatOk:
            warning('invalid-syntax', line=ligne)
            self.status = ReponsesStatus.INVALID_SYNTAX
            return
        contenu += "\n"
        self.data = {}
        continuer = True
        indexLecture = 0
        self.état_initial()
        while indexLecture < len(contenu):
            char = contenu[indexLecture]
            if self.état == "filtre":
                if not (char.isalpha() or char.isdigit()):
                    indexLecture += 1
                elif char in NEWLINES:
                    self.état_initial()
                else:
                    self.état_suivant()
            elif self.état == "nombre":
                if char.isdigit():
                    self.buffer.append(char)
                    indexLecture += 1
                elif char in NEWLINES:
                    self.état_initial()
                else:
                    self.état_suivant()
            elif self.état == "lettre":
                if char.isalpha():
                    self.buffer.append(char)
                    indexLecture += 1
                else:
                    self.état_suivant()
