from kivy.app import App
from kivy.factory import Factory
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.properties import ObjectProperty, StringProperty, ColorProperty

from automate import Automate
from reponses import ReponsesStatus
from outils import xdg_open, ui_message
from errors import UserError

class ErrorHandler:

    def __call__(self, function):
        def wrap(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except UserError as error:
                self.handler(error)
        return wrap

handle_user_error = ErrorHandler()



class OpenDialog(FloatLayout):
    choose_file = ObjectProperty(None)
    cancel = ObjectProperty(None)

class ErrorDialog(FloatLayout):
    message = StringProperty()
    cancel = ObjectProperty(None)

class Resultline(BoxLayout):
    student_name = StringProperty()
    student_status = StringProperty()
    student_copie = StringProperty()
    student_recompute = ObjectProperty(None)
    color = ColorProperty()

class Application(App):
    qcmfile = StringProperty('')
    archivefile = StringProperty('')
    studentlist = ObjectProperty(None)

    def __init__(self):
        super().__init__(icon='icon_lowres.png', title='Caquer')
        self.automate = Automate()

    def dismiss_popup(self):
        self._popup.dismiss()

    def dismiss_error_popup(self):
        self._error_popup.dismiss()

    def warn_user(self, error):
        content = ErrorDialog(message=ui_message(str(error)),
                              cancel=self.dismiss_error_popup)
        self._error_popup = Popup(title='Erreur', content=content,
                                  size_hint=(0.8, 0.8))
        self._error_popup.open()

    def open_file(self, filetype):
        TITLES = {'qcm': 'Choisir un fichier de qcm',
                  'archive': 'Choisir le fichier d’archive contenant les devoirs'}
        content = OpenDialog(choose_file=self.set_file(filetype),
                             cancel=self.dismiss_popup)
        self._popup = Popup(title=TITLES[filetype], content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def set_file(self, filetype):
        def setter(filename):
            if filename:
                if filetype == 'qcm':
                    self.qcmfile = filename[0]
                else:
                    self.archivefile = filename[0]
            self.dismiss_popup()
        return setter

    def update_line_status(self, line, status):
        line.student_status = status.name
        if status == ReponsesStatus.VALID:
            line.color = (0, 1, 0.2, 1)
        else:
            line.color = (1, 0.2, 0, 1)

    @handle_user_error
    def corriger(self):
        self.automate.corriger(self.qcmfile, self.archivefile)
        self.root.current = 'menu_resultats'
        studentlist = self.root.current_screen.studentlist
        for eleve in self.automate.classeCourante:
            line = Resultline(student_name=eleve.présenter(),
                              student_copie=eleve.réponses.cheminFichier,
                              student_recompute=self.recorriger_eleve(eleve),
                              size_hint_y=None,
                              height=40)
            self.update_line_status(line, eleve.réponses.status)
            studentlist.add_widget(line)
        studentlist.height = 40*len(self.automate.classeCourante)

    @handle_user_error
    def verifier_qcm(self):
        ficheRemarques = self.automate.vérifier_qcm(self.qcmfile)
        xdg_open(ficheRemarques)

    def recorriger_eleve(self, eleve):
        @handle_user_error
        def func(line):
            self.automate.recorriger_élève(eleve, self.automate.classeCourante)
            self.update_line_status(line, eleve.réponses.status)
        return func

if __name__ == '__main__':
    Factory.register('OpenDialog', cls=OpenDialog)
    Factory.register('ErrorDialog', cls=ErrorDialog)
    Factory.register('Resultline', cls=Resultline)


    app = Application()
    handle_user_error.handler = app.warn_user
    app.run()
