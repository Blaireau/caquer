COMMANDES = {
    "créer": {
            "commande": "commande_créer",
            "args": 1,
            "aide": """Créer un nouveau fichier de qcm, l'enregistrer et l'ouvrir.
    créer <nomFichier>
    Exemple : `=> créer fonctions` """
    },
    "ouvrir": {
        "commande": "commande_ouvrir",
        "args": 1,
        "aide": """Ouvre un fichier qcm en tant que fichier courant.
ouvrir <nomFichier>
Exemple : `=> ouvrir fonctions` """
    },
    "sauver": {
        "commande": "commande_sauver",
        "args": 0,
        "aide": """Enregistre le fichier courant.
sauver
Exemple : `=> sauver`"""
    },
    "corriger": {
        "commande": "commande_corriger",
        "args": 0,
        "aide": """Corrige le qcm courant
corriger
OU
corriger <qcm> [extraire]
Exemple : `=> corriger qcmProba`

Avec l’option 'extraire' activée, les réponses sont extraites de l’archive zip.
""",
    },
    "voirCorrigé":{
        "commande": "commande_voir_corrigé",
        "args": 0,
        "aide": """Voir toutes les indications d’un qcm
voirCorrigé
OU
voirCorrigé <qcm> """
    },
    "aide": {
        "commande": "commande_aide",
        "args": 1,
        "aide": """Pour obtenir de l'aide, quoi ! ;)"""
    },
    "liste": {
        "commande": "commande_liste",
        "args": 0,
        "aide": """Liste les commandes existantes"""
    },
    "quitter": {
        "commande": "commande_quitter",
        "args": 0,
        "aide": """Quitte le programme. Pas de paramètres."""
    }
}
